// console.log('Hello World');

// [Section] Function
// Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called or invoke.
// Functions are mostly created to create a complicated tasks to run several lines of code in succession.
// They are also used to prevent repeating lines/block of codes that perform the same task or function.

// Function declaration
// regular function (function declaration, function expression) and arrow funtion

// function expression
		// anonymous function
		const greeting = function(name, time) {
			console.log(`Good ${time}, ${name}.`);
		}

		let m = 'Chris';
		greeting(m, 'day');

		// function expression with name
		const greeting2 = function functionName(name, time) {
			console.log(`Good ${time}, ${name}.`);
		}

		greeting2('Christopher', 'morning');

//function declaration
greet();

function greet() {
	console.log('Hello World.');
}

greet();

// arrow function
// this is a regular function (declaration)
const greets1 = function(name) {
	console.log(`Hi there ${name}.`);
}

// this is an arrow function (change 'function' to =>)
const greets2 = (name) => {
	console.log(`Hi there ${name}.`);
}

// if only 1 parameter, can be written as
const greets3 = () => {
	console.log(`Hi there ${name}.`);
}

greets1('John');
greets1('David');
greets1('Mark');

// Function Scoping
let namex = 'Shula';

function names() {
	console.log(namex);
}

names();

//Nested funtions
function myNewFunction() {
	let n = 'Jane';
	console.log(n);

	function nestedFunction() {
		let nestedName = 'Marry';

		console.log(nestedName);
		console.log(n);
	}

	nestedFunction();
}

myNewFunction();



// Prompt
prompt('hello');

function welcome() {
	let fName = prompt('Enter first name:');
	let lName = prompt('Enter last name');
	console.log(`Hello ${fName} ${lName}.`);
}

welcome();